<html>
<head>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/app.css?v=11">
    <link rel="stylesheet" href="/css/menu.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    @include("layouts/fontsGoogle")
    <link rel="icon" type="image/png" href="/images/home_logo.png" />
    <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/general.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    @yield('head')
    @include('layouts.mensajesJs')
    @include('layouts.fineUploaderHeader')
    <title>@yield('title')</title>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body class="bodyconMenu">
<div>
    <div id="divMenuSuperior">
        <ul>
        @foreach($opcionesMenu as $opcionMenu)
                @if("/".Request::path()==$opcionMenu->link)
                    <li><a class="opcionSeleccionada" href="{{$opcionMenu->link}}">{{ucfirst(strtolower(trans($opcionMenu->titulo)))}}</a></li>
                @else
                    <li><a href="{{$opcionMenu->link}}">{{ucfirst(strtolower(trans($opcionMenu->titulo)))}}</a></li>
                @endif
            @endforeach
        </ul>
    </div>

    <div class="divUser">
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
            @else

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }}&#x2304;
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</div>
@section('header')
@show
<div id="mensajes"></div>
<div class="contenidoPaginaGenerica">
    @yield('content')
</div>
@section('footer')
@show
</body>
</html>