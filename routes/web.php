<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// ========= GET

// ========= POST

// ========= PUT

// ========= DELETE

// ========= GENERICAS
Route::get('/api/{nombreModelo}', 'DefaultModelController@getAll')->middleware('apiKey');
Route::get('/noPermission', 'DefaultModelController@noPermission');
Route::get('/api/{nombreModelo}/{id}', 'DefaultModelController@getById')->middleware('apiKey');
Route::put('/api/{nombreModelo}', 'DefaultModelController@insertModel')->middleware('apiKey');
Route::delete('/api/{nombreModelo}/{id}', 'DefaultModelController@deleteModel')->middleware('apiKey');
Route::delete('/api/{nombreModelo}', 'DefaultModelController@deleteModelsByFilter')->middleware('apiKey');
Route::post('/api/login', 'DefaultModelController@login')->middleware('apiKey');



