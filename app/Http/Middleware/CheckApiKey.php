<?php

namespace Owe\Http\Middleware;

use Closure;

class CheckApiKey
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey=env("APP_KEY");
        $useApiKey=env("APP_USE_API_KEY");
        if($useApiKey)
        {
            if($request->has("apiKey"))
            {
                if($apiKey==$request->apiKey)
                {
                    return $next($request);
                }else
                {
                   return redirect('/noPermission');
                }
            }else
            {
                return redirect('/noPermission');
            }
        }else
        {
            return $next($request);
        }
    }
}
