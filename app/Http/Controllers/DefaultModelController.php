<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Psy\Util\Json;

class DefaultModelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function noPermission()
    {
        return trans("auth.no_permission");
    }
    //Recibiré el modelo, opcionalmente un objeto de filtro "filters" ,
    //page: página
    //rowsPerPage: cantidad es opcional, si no viene utilizaremos un valor por defecto
    //relatedModels: array con nombre de relaciones que queremos que también se carguen
    public function getAll($modelName, Request $request)
    {

        $result["result"] = "ok";

        try {
            $filters = Array();
            $fullName = "Owe\\" . $modelName;
            if ($request->has("filters")) {
                $filters = json_decode($request->get("filters"));
                if ($filters == null)
                    $filters = Array();
            }
            $with = Array();
            if ($request->has("relatedModels")) {
                $with = json_decode($request->get("relatedModels"));
                if ($with == null)
                    $with = Array();
            }
            $rowsPerPage = $fullName::$rowsPerPage;
            if ($request->has("rowsPerPage"))
                $rowsPerPage = $request->get("rowsPerPage");

            $orderBy=Array();
            if($request->has("orderBy"))
            {
                $orderBy=json_decode($request->get("orderBy"));
            }

            $query = $fullName::buildQuery($filters);
            $query->with($with);

            foreach ($orderBy as $sqlOrder)
            {
                $query->orderBy($sqlOrder->field,$sqlOrder->orden);
            }
            if ($rowsPerPage > 0)
                $response = $query->paginate($rowsPerPage);
            else
                $response = $query->get();
            $result["response"] = $response;
        } catch (Exception $e) {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        } catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        }finally {
            return json_encode($result);
        }
    }

    public function getById($modelName, $id, Request $request)
    {
        $with = Array();
        if ($request->has("relatedModels")) {
            $with = json_decode($request->get("relatedModels"));
            if ($with == null)
                $with = Array();
        }
        $result["result"] = "ok";
        try {
            $fullName = "Owe\\" . $modelName;
            $result["response"] = $fullName::with($with)->find($id);
        } catch (Exception $e) {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        }catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        } finally {
            return json_encode($result);
        }
    }

    public function deleteModel($modelName, $id)
    {
        $result["result"] = "ok";
        try {
            $fullName = "Owe\\" . $modelName;
            $model = $fullName::find($id);
            $model->delete();
        } catch (Exception $e) {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        }catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        } finally {
            return json_encode($result);
        }
    }

    public function deleteModelsByFilter(Request $request, $modelName)
    {
        $result["result"] = "ok";
        try {
            $fullName = "Owe\\" . $modelName;
            $jsonParams = json_decode($request->getContent(), false);
            if (array_has($jsonParams, "filters")) {
                $filters = $jsonParams->filters;
                if ($filters == null)
                    throw new Exception("Must provide a filter");
                $query = $fullName::buildQuery($filters);
                $query->delete();
            } else {
                throw new Exception("Must provide a filter");
            }

        } catch (Exception $e) {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        }catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        } finally {
            return json_encode($result);
        }
    }

    public function insertModel($modelName, Request $request)
    {
        $result["result"] = "ok";
        try {
            $inputModelsRelated=Array();
            $jsonDatos = $request->json()->all();
            foreach ($jsonDatos['models'] as $input) {
                $fullName = "Owe\\" . $modelName;
                if (array_has($input, 'id'))
                    $modelo = $fullName::firstOrNew(['id' => $input['id']]);
                else
                    $modelo = new $fullName();

                if(array_has($jsonDatos, 'relatedModels'))
                {
                    foreach ($jsonDatos["relatedModels"] as $modelRelated)
                    {
                        $inputModelsRelated[$modelRelated]=$input[$modelRelated];
                        unset($input[$modelRelated]);
                    }
                }
                $modelo->fill($input);
                $operacion = "i";//Asumo que es insert hasta comprobar si existe el registro
                if ($modelo->exists) {
                    $operacion = "u";
                }
                $modelo->saveOrFailWithTriggers($operacion);
                if(array_has($jsonDatos, 'relatedModels'))
                {
                    foreach ($jsonDatos["relatedModels"] as $modelRelated)
                    {
                        $modelsRelatedNew=Array();
                        foreach($inputModelsRelated[$modelRelated] as $inputRelated)
                        {
                            $modelRelatedFull="Owe\\" . $modelRelated;
                            $modelRelatedNew=new $modelRelatedFull();
                            $modelRelatedNew->fill($inputRelated);
                            $modelsRelatedNew[]=$modelRelatedNew;
                        }
                        $modelo->$modelRelated()->saveMany($modelsRelatedNew);
                        foreach ($modelsRelatedNew as $modelRelated)
                        {
                            foreach($modelRelated->triggersInsert as $triggerInsert)
                            {
                                $modelRelated->$triggerInsert();
                            }
                        }
                    }
                }
            }
        } catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        }catch(\Exception $e)
        {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        }finally {
            return json_encode($result);
        }
    }

    public function login(Request $request)
    {
        $result["result"] = "ok";

        try {
            $jsonDatos = $request->json()->all();
            $inputs=$jsonDatos["model"];
            $user=Users::where("email",$inputs["email"])->first();
            if(sizeof($user)>0)
            {
                switch ($jsonDatos["login_type"])
                {
                    case "email":
                    {
                        if(Hash::check($inputs["password"], $user->password))
                        {
                            $user->password="Private data";
                            $result["response"]=$user;
                        }else
                        {
                            $result["result"] = "error";
                            $result["mesage"] = "Error password";
                        }
                        break;
                    }
                    case "google":
                    {
                        $userGoogle=UsersGoogle::where("user_id",$user->id)->first();
                        if(sizeof($userGoogle)==0)
                        {
                            $this->insertAccountGoogle($inputs,$user->id);
                        }
                        $user->password="Private data";
                        $result["response"]=$user;
                        break;
                    }

                    case "facebook":
                    {
                        $userFacebook=UsersFb::where("user_id",$user->id)->first();
                        if(sizeof($userFacebook)==0)
                        {
                            $this->insertAccountFacebook($inputs,$user->id);
                        }
                        $user->password="Private data";
                        $result["response"]=$user;
                        break;
                    }
                }
            }else
            {
                $user=new Users();
                switch ($jsonDatos["login_type"])
                {
                    case "email":
                    {
                        $result["result"] = "error";
                        $result["mesage"] = "error email";
                        break;
                    }
                    case "google":
                    {
                        $user->name=$inputs["name"];
                        $user->surnames=$inputs["family_name"];
                        $user->email=$inputs["email"];
                        $user->saveOrFailWithTriggers('i');
                        $this->insertAccountGoogle($inputs,$user->id);
                        $user->password="Private data";
                        $result["response"]=$user;
                        break;
                    }
                    case "facebook":
                    {
                        $user->name=$inputs["first_name"];
                        $user->surnames=$inputs["last_name"];
                        $user->email=$inputs["email"];
                        $user->saveOrFailWithTriggers('i');
                        $this->insertAccountFacebook($inputs,$user->id);
                        $user->password="Private data";
                        $result["response"]=$user;
                        break;
                    }
                }
            }
        } catch (Exception $e) {
            $result["result"] = "error";
            $result["mesage"] = $e->getMessage();
        } catch(Throwable $er)
        {
            $result["result"] = "error";
            $result["mesage"] = $er->getMessage();
        }finally {
            return json_encode($result);
        }
    }

    function insertAccountFacebook($inputs,$userId)
    {
        $userFacebook=new UsersFb();
        $userFacebook->user_id=$userId;
        $userFacebook->birthday=$inputs["birthday"];
        $userFacebook->first_name=$inputs["first_name"];
        $userFacebook->gender=$inputs["gender"];
        $userFacebook->last_name=$inputs["last_name"];
        $userFacebook->link=$inputs["link"];
        $userFacebook->locale=$inputs["locale"];
        $userFacebook->facebook_id=$inputs["facebook_id"];
        $userFacebook->saveOrFailWithTriggers('i');
    }

    function insertAccountGoogle($inputs,$userId)
    {
        $userGoogle=new UsersGoogle();
        $userGoogle->user_id=$userId;
        $userGoogle->name=$inputs["name"];
        $userGoogle->picture=$inputs["picture"];
        $userGoogle->given_name=$inputs["given_name"];
        $userGoogle->family_name=$inputs["family_name"];
        $userGoogle->locale=$inputs["locale"];
        $userGoogle->google_id=$inputs["google_id"];
        $userGoogle->token_id=$inputs["token_id"];
        $userGoogle->saveOrFailWithTriggers('i');
    }

    public function registerMail(Request $request)
    {
        $result["result"] = "ok";
        $jsonDatos = $request->json()->all();
        $inputs=$jsonDatos["model"];
        $user=Users::where("email",$inputs["email"])->first();
        if(sizeof($user)>0)
        {
            $result["result"] = "error";
            $result["mesage"] = "The user already exists with this email";
        }else
        {
            if($inputs["email"]==""||$inputs["password"]=="")
            {
                $result["result"] = "error";
                $result["mesage"] = "Data is missing for registration";
            }else
            {
                $user=new Users();
                $inputs["password"]=Hash::make($inputs["password"]);
                $user->fill($inputs);
                $user->saveOrFailWithTriggers('i');
                $result["response"]=$user;
            }

        }
        return json_encode($result);
    }
}
?>