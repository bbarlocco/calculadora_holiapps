<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    //
    /**
     * Get the connection of the entity.
     *
     * @return string|null
     */
    //Permito hacer fill de todos los campos
    protected $guarded = array();
    //Cantidad de registros por página que se mostrarán para el modelo por defecto, se puede sobreescribir en los modelos
    public static $rowsPerPage=20;

    //Triggers!
    public $triggersUpdate;
    public $triggersInsert;
    public $triggersDelete;

    public $urlServer="https://s3-us-west-2.amazonaws.com/stadiumallain/";

    public function getQueueableConnection()
    {
        // TODO: Implement getQueueableConnection() method.
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        // TODO: Implement resolveRouteBinding() method.
    }

    //Utilizaremos siempre este para guardar, así ejecuta los triggers programados
    public function saveOrFailWithTriggers($action)
    {
        if($action=="a")
        {
            if($this->triggersUpdate!=null){
                foreach($this->triggersUpdate as $metodo){
                    $this->$metodo();
                }

            }

        }

        $resultado=$this->saveOrFail();

        if($action=="i")
        {
            if($this->triggersInsert!=null)
                foreach($this->triggersInsert as $metodo)
                    $this->$metodo();
        }

        return $resultado;
    }
    //Obtener todas las columnas
    public function getColumnListing(){
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    //Relaciones anotadas
    public static function getSupportedRelations()
    {
        echo "a1";
        $relations = [];
        echo get_called_class();
        $reflextionClass = new ReflectionClass(get_called_class());
        echo "a2";
        foreach($reflextionClass->getMethods() as $method)
        {
            $doc = $method->getDocComment();

            if($doc && strpos($doc, '@Relation') !== false)
            {
                $relations[] = $method->getName();
            }
        }
        echo "a3";
        return $relations;
    }
    //Construir query en base a un JSON de filtro
    //estructura para el filtro: [{'filterBy':'fieldName','value':'value','op':'op','rel':'rel'}]
    //ejemplo [{'filterBy':'name','value':'bruno','op':'like','rel':'and'},{'filterBy':'id','value':[1,2,3],'op':'in','rel':'or'}]
    //Operadores relacionales: '=', '<', '>', '<=', '>=', '<>', '!=', 'like', 'not like', 'between', 'ilike'
    //Relaciones where(rel):'and','or'
    //
    //
    static public function buildQuery($params){

        $query=(new static)->newQuery();
        foreach($params as $param){

            if($param->op!="in")
            {
                if($param->rel=="and")
                {
                    $query->where($param->filterBy,$param->op,$param->value);
                }else if($param->rel=="or")
                {
                    $query->orWhere($param->filterBy,$param->op,$param->value);
                }

            } else
                $query->whereIn($param->filterBy,$param->value);
        }
        return $query;
    }
}
